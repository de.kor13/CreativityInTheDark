import * as PIXI from "pixi.js";
import { convertTypeAcquisitionFromJson } from "typescript";

export default class Game {
  private app: PIXI.Application;
  private hero: PIXI.Sprite;
  private speed: number = 20; // скорость движения спрайта

  constructor(canvas: HTMLCanvasElement) {
    this.app = new PIXI.Application({
      view: canvas,
      width: window.innerWidth,
      height: window.innerHeight,
      backgroundColor: 0x1099bb,
    });
  }

  public start(): void {
    // Загрузка изображения
    const texturePromise = PIXI.Assets.load('/assets/images/Hero126.png');

    // When the promise resolves, we have the texture!
    texturePromise.then((resolvedTexture) => {
      // create a new Sprite from the resolved loaded Texture
      this.hero = PIXI.Sprite.from(resolvedTexture);

      // center the sprite's anchor point
      this.hero.anchor.set(0.5);

      // move the sprite to the center of the screen
      this.hero.x = this.app.screen.width / 2;
      this.hero.y = this.app.screen.height / 2;

      this.app.stage.addChild(this.hero);

      // добавляем обработчик событий клавиатуры
      window.addEventListener("keydown", this.handleKeyDown.bind(this));
      window.addEventListener("keyup", this.handleKeyUp.bind(this));
    });
  }

  // Обработчик события нажатия клавиши
  private handleKeyDown(event: KeyboardEvent) {
    switch (event.code) {
      case "KeyW":
        this.hero.y -= this.speed;
        break;
      case "KeyS":
        this.hero.y += this.speed;
        break;
      case "KeyA":
        this.hero.x -= this.speed;
        break;
      case "KeyD":
        this.hero.x += this.speed;
        break;
    }
  }

  // Обработчик события отпускания клавиши
  private handleKeyUp(event: KeyboardEvent) {
    switch (event.code) {
      case "KeyW":
        this.hero.y += this.speed;
        break;
      case "KeyS":
        this.hero.y -= this.speed;
        break;
      case "KeyA":
        this.hero.x += this.speed;
        break;
      case "KeyD":
        this.hero.x -= this.speed;
        break;
    }
  }
}

